import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

interface ButtonProps {
  openModal: (actionIsEdit: boolean) => void;
}

const AddButton = ({ openModal }: ButtonProps) => {
  return (
    <button className="add-button" onClick={() => openModal(false)}>
      <FontAwesomeIcon icon={faPlus} />
    </button>
  );
};

export default AddButton;
