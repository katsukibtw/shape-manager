import { useRef, useState } from "react";
import "./Modal.css";
import { useShapesContext } from "../../store";

interface ModalProps {
  closeModal: () => void;
  willModalEdit: boolean;
  shapeId?: number;
}

const Modal = ({ closeModal, willModalEdit, shapeId }: ModalProps) => {
  const { addShape, getShapeById, editShape } = useShapesContext();

  const shape = getShapeById(shapeId!);

  const [isSquare, setIsSquare] = useState<boolean>(false);

  const sizeInputRef = useRef<HTMLInputElement>(null);
  const colorInputRef = useRef<HTMLInputElement>(null);
  const xPosInputRef = useRef<HTMLInputElement>(null);
  const yPosInputRef = useRef<HTMLInputElement>(null);

  const onModalClick = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  const onSubmit = () => {
    if (!sizeInputRef.current || !colorInputRef.current) return;

    if (!willModalEdit) {
      const latestShapeId = Number(localStorage.getItem("latestShapeId")) || 1;
      localStorage.setItem("latestShapeId", (latestShapeId + 1).toString());

      addShape({
        name: isSquare ? `square_${latestShapeId}` : `sircle_${latestShapeId}`,
        isSquare: isSquare,
        size: Number(sizeInputRef.current.value),
        backgroundColor: colorInputRef.current.value,
        xPos: Number(xPosInputRef.current?.value),
        yPos: Number(yPosInputRef.current?.value),
      });
    } else {
      editShape(
        shapeId!,
        Number(sizeInputRef.current.value),
        colorInputRef.current.value,
      );
    }

    closeModal();
  };

  return (
    <div onClick={closeModal} className="modal">
      <div className="modal__container" onClick={onModalClick}>
        <h2>
          {willModalEdit
            ? `Редактироватe параметры фигуры ${shape?.name}`
            : "Создать фигуру"}
        </h2>
        {!willModalEdit && (
          <div className="tp-selector">
            <span
              style={{
                left: isSquare ? "calc(50% - 0.25rem)" : "0.25rem",
              }}
            ></span>
            <div
              className="tp-selector__item"
              onClick={() => setIsSquare(false)}
            >
              Круг
            </div>
            <div
              className="tp-selector__item"
              onClick={() => setIsSquare(true)}
            >
              Квадрат
            </div>
          </div>
        )}
        <label htmlFor="size">Размер:</label>
        <input
          type="number"
          placeholder="Впишите число"
          id="size"
          ref={sizeInputRef}
          defaultValue={shape?.size || 1}
        />
        <label htmlFor="color">Цвет:</label>
        <input
          type="text"
          placeholder="Впишите код цвета в hex, rgb или hsl"
          id="color"
          ref={colorInputRef}
          defaultValue={shape?.backgroundColor || "#f00"}
        />
        {!willModalEdit && (
          <>
            <label htmlFor="xPos">Координата X:</label>
            <input
              type="number"
              placeholder="Впишите число"
              id="xPos"
              ref={xPosInputRef}
              defaultValue={shape?.xPos || 0}
            />
            <label htmlFor="size">Координата Y:</label>
            <input
              type="number"
              placeholder="Впишите число"
              id="yPos"
              ref={yPosInputRef}
              defaultValue={shape?.yPos || 0}
            />
          </>
        )}
        <button onClick={onSubmit}>
          {willModalEdit ? "Сохранить" : "Создать"}
        </button>
      </div>
    </div>
  );
};

export default Modal;
