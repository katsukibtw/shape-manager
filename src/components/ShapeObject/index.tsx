import { useRef } from "react";
import { Shape } from "../../shape.interfaces";
import { useShapesContext } from "../../store";
import "./ShapeObject.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPenToSquare } from "@fortawesome/free-solid-svg-icons";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";

interface ShapeObjectProps {
  shape: Shape;
  index: number;
  openModal: (actionIsEdit: boolean, shapeId?: number) => void;
}

const ShapeObject = ({ shape, index, openModal }: ShapeObjectProps) => {
  const { deleteShape, updateShapeCoords } = useShapesContext();

  const nodeRef = useRef<HTMLDivElement>(null);

  const onStop = (e: DraggableEvent, data: DraggableData) => {
    e.preventDefault();
    updateShapeCoords(data.x, data.y, index);
  };

  return (
    <Draggable
      bounds="body"
      onStop={onStop}
      nodeRef={nodeRef}
      cancel="button"
      defaultPosition={{ x: shape.xPos, y: shape.yPos }}
    >
      <div className="shape_container" ref={nodeRef}>
        <div
          style={{
            backgroundColor: shape.backgroundColor,
            width: `${shape.size}rem`,
            height: `${shape.size}rem`,
            borderRadius: shape.isSquare ? "0.5rem" : "50%",
          }}
          className="shape"
        >
          {shape.name}
        </div>
        <button
          className="shape__btn"
          onClick={() => openModal(true, index)}
          style={{ right: "2.5rem" }}
        >
          <FontAwesomeIcon icon={faPenToSquare} />
        </button>
        <button className="shape__btn" onClick={() => deleteShape(index)}>
          <FontAwesomeIcon icon={faTrash} />
        </button>
      </div>
    </Draggable>
  );
};

export default ShapeObject;
