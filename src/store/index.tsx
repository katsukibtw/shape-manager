import { Shape, ShapesContextType } from "../shape.interfaces";
import { PropsWithChildren, createContext, useContext, useState } from "react";

export const ShapesContext = createContext<ShapesContextType | null>(null);

const ShapesProvider = ({ children }: PropsWithChildren) => {
  const [shapes, setShapes] = useState<Shape[]>([]);

  const addShape = (payload: Shape): void => {
    setShapes((state) => [...state, payload]);
  };

  const deleteShape = (shapeId: number): void => {
    setShapes((state) => state.filter((_, index) => index !== shapeId));
  };

  const updateShapeCoords = (
    xPosNew: number,
    yPosNew: number,
    shapeId: number,
  ) => {
    setShapes((state) =>
      state.map((el, id) =>
        id === shapeId ? { ...el, xPos: xPosNew, yPos: yPosNew } : el,
      ),
    );
  };

  const getShapeById = (id: number): Shape => {
    return shapes[id];
  };

  const editShape = (
    id: number,
    newSize: number,
    newBackgroundColor: string,
  ) => {
    setShapes((state) =>
      state.map((el, index) =>
        id === index
          ? {
              ...el,
              size: newSize,
              backgroundColor: newBackgroundColor,
            }
          : el,
      ),
    );
  };

  return (
    <ShapesContext.Provider
      value={{
        shapes,
        addShape,
        deleteShape,
        updateShapeCoords,
        getShapeById,
        editShape,
      }}
    >
      {children}
    </ShapesContext.Provider>
  );
};

export default ShapesProvider;

export const useShapesContext = () =>
  useContext(ShapesContext) as ShapesContextType;
