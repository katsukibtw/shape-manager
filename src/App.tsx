import "./App.css";
import ShapeObject from "./components/ShapeObject";
import AddButton from "./components/AddButton";
import { useRef, useState } from "react";
import { useShapesContext } from "./store";
import Modal from "./components/Modal";

const App = () => {
  const { shapes } = useShapesContext();

  const [isModalOpened, setIsModalOpened] = useState<boolean>(false);
  const willModalEdit = useRef<boolean>(false);
  const shapeIdToEdit = useRef<number>(-1);

  const openModal = (actionIsEdit: boolean, shapeId?: number): void => {
    willModalEdit.current = actionIsEdit;
    shapeIdToEdit.current = shapeId!;

    setIsModalOpened(true);
  };

  const closeModal = (): void => {
    setIsModalOpened(false);
  };

  return (
    <>
      <header>Shape manager</header>
      <AddButton openModal={openModal} />
      {isModalOpened &&
        (shapeIdToEdit.current >= 0 ? (
          <Modal
            closeModal={closeModal}
            willModalEdit={willModalEdit.current}
            shapeId={shapeIdToEdit.current}
          />
        ) : (
          <Modal
            closeModal={closeModal}
            willModalEdit={willModalEdit.current}
          />
        ))}
      <div className="shapes-box">
        {shapes.length === 0 ? (
          <div className="placeholder">Нажмите на +, чтобы добавить фигуру</div>
        ) : (
          shapes.map((el, index) => (
            <ShapeObject
              shape={el}
              key={index}
              index={index}
              openModal={openModal}
            />
          ))
        )}
      </div>
    </>
  );
};

export default App;
