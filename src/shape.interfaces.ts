export interface Shape {
  readonly name: string;
  readonly isSquare: boolean;
  size: number;
  backgroundColor: string;
  xPos: number;
  yPos: number;
}

export interface ShapesContextType {
  shapes: Shape[];
  addShape: (payload: Shape) => void;
  deleteShape: (shapeId: number) => void;
  updateShapeCoords: (
    xPosNew: number,
    yPosNew: number,
    shapeId: number,
  ) => void;
  getShapeById: (id: number) => Shape;
  editShape: (id: number, newSize: number, newBackgroundColor: string) => void;
}
